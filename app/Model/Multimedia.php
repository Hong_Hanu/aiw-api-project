<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Tag;
use Cviebrock\EloquentSluggable\Sluggable;
class Multimedia extends Model
{
    use Sluggable;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'multimedia';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =["title","short_des","content","title","link","author","category_id"];

    /**
     * The tag that belong to the Multimedia.
     */
    public function tag(){
        return $this->belongsToMany(Tag::class);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(){
        return [
            'slug' => [
                'source'=> 'title'
            ]
        ];
    }
}
