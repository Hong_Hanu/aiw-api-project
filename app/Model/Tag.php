<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Multimedia;
use App\Model\News;
class Tag extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tag';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =["name"];

    /**
     * The multimedia that belong to the Tag.
     */
    public function multimedia(){
        return $this->belongsToMany(Multimedia::class)->withTimestamps();
    }

    /**
     * The news that belong to the Tag.
     */
    public function news(){
        // return $this->belongsToMany(News::class)->withTimestamps();
        return $this->belongsToMany(News::class)->withTimestamps();
    }

}
