<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\News;

class Category extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'category';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =["name"];

    /*
     * Get List of news from data for category
     * A "one-to-many" relationship is used to define relationships where a single model owns any amount of other models
     *
     */
    public function news(){
        return $this->hasMany(News::class);
    }
}
