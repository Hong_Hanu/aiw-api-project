<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Category;
use App\Model\Tag;
use Cviebrock\EloquentSluggable\Sluggable;


class News extends Model
{
    use Sluggable;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ["title", "short_des", "content", "title", "thumb", "author", "category_id"];

    /**
     * The category that belong to the News.
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * The tag that belong to the News.
     */
    public function tag()
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * The comment that belong to the News.
     */
    public function newsComment()
    {
        return $this->hasMany(NewsComment::class);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
