<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Multimedia;
use App\Model\News;

class NewsComment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news_comment';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ["author", "content", "news_id"];

    /**
     * The news that belong to the newsComment.
     */
    public function news()
    {
        return $this->belongsTo(News::class)->withTimestamps();
    }
}
