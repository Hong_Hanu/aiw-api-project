<?php
namespace App\Http\Controllers;
use App\Http\Controllers\ApiController;
use App\Http\Transformer\MultimediaDetailTransformer;
use App\Http\Transformer\MultimediaTransformer;
use App\Model\Multimedia;
class MultimediaController extends ApiController{

    /**
     * Show lists of multimedia
     *
     * @return Response
     */
    public function index(){
        $muls = Multimedia::paginate(4);
        return $this->respondWithPaginator($muls, new MultimediaTransformer());
    }

    /**
     * Show detail of multimedia from slug
     *
     * @return Response
     */
    public function show($slug){
        $muls = Multimedia::where('slug','=',$slug)->get();
        return $this->respondWithCollection($muls, new MultimediaDetailTransformer());
    }
}