<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Response;
use League\Fractal;
use App\Http\Requests;
use App\Model\News;
use App\Model\Category;
use App\Http\Transformer\NewsTransformer;
use App\Http\Transformer\NewsDetailTransformer;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Http\Controllers\ApiController;

class NewsController extends ApiController
{
    /**
     * Show lists of news
     *
     * @return Response
     */
    public function index()
    {
        $news = News::orderBy('created_at','desc')->paginate(2);
        return $this->respondWithPaginator($news, new NewsTransformer());
    }

    /**
     * Show detail of news from slug
     *
     * @param string $slug
     * @return Response
     */
    public function show($slug){
        $news = News::where('slug',$slug)->get();
        return $this->respondWithCollection($news, new NewsDetailTransformer());
    }

}
