<?php
namespace App\Http\Controllers;
use App\Http\Controllers\ApiController;
use App\Http\Transformer\CategoryTransformer;
use App\Model\Category;
use App\Model\News;
use App\Http\Transformer\SearchTransformer;
class CategoryController extends ApiController{
    /**
     * Show lists of categories
     *
     * @return Response
     */
    public function index()
    {
        $categories = Category::all();
        return $this->respondWithCollection($categories, new CategoryTransformer());
    }

    /*
    * Show lists of new for category
    *
    * @param  string  $name
    * @return Response
    *
    */
    public function show($name){
        $cat = Category::where('name',$name)->get();

        foreach ($cat as $c) {
        	$result = $c->news()->orderBy('created_at','desc')->paginate(3);	
        }
        return $this->respondWithPaginator($result, new SearchTransformer());
    }
}