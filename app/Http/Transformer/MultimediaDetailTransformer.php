<?php
namespace App\Http\Transformer;
use App\Model\Multimedia;
use League\Fractal;
class MultimediaDetailTransformer extends Fractal\TransformerAbstract{

    /**
     * @param $muls
     * @return mixed
     */
    public function transform(Multimedia $muls)
    {
        $created_at= strtotime($muls->created_at);
        $created_at = date("F j, Y, g:i a",$created_at);
        return [
            "id" => $muls->id,
            "title" => $muls->title,
            "content" => $muls->content,
            "category_id" => $muls->category_id,
            "slug" => $muls->slug,
            "thumb" => $muls->thumb,
            "author" => $muls->author,
            "created_at" => $created_at
        ];
    }
}