<?php
namespace App\Http\Transformer;
use App\Model\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
    /**
     * @param $categories
     * @return mixed
     */
    public function transform(Category $categories)
    {
        return [
            "id" => $categories->id,
            "name" => $categories->name,
        ];
    }
}